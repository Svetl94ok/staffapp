﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StaffApp.Models;
using BusinessLogicLayer.DataTransferObjects;
using System.Collections.Generic;
using BusinessLogicLayer.Infrastructure;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Owin.Security;
using StaffApp.ViewModels;
using Microsoft.AspNetCore.Server.HttpSys;

namespace StaffApp.Controllers
{
        public class AccountController : Controller
        {
        private UserService userService;



        public ActionResult Login()
            {
                return View();
            }

            //[HttpPost]
            //[ValidateAntiForgeryToken]
            //public async Task<ActionResult> Login(LoginViewModel model)
            //{
            //    await SetInitialDataAsync();
            //    if (ModelState.IsValid)
            //    {
            //        UserDTO userDto = new UserDTO { Email = model.Email, Password = model.Password };
            //        ClaimsIdentity claim = await UserService.Authenticate(userDto);
            //        if (claim == null)
            //        {
            //            ModelState.AddModelError("", "Incorrect login or password.");
            //        }
            //        else
            //        {
            //            AuthenticationManager.SignOut();
            //            AuthenticationManager.SignIn(new AuthenticationProperties
            //            {
            //                IsPersistent = true
            //            }, claim);
            //            return RedirectToAction("Index", "Home");
            //        }
            //    }
            //    return View(model);
            //}

            //public ActionResult Logout()
            //{
            //    AuthenticationManager.SignOut();
            //    return RedirectToAction("Index", "Home");
            //}



            [HttpPost]
            [ValidateAntiForgeryToken]
            public async Task<ActionResult> Register(RegisterViewModel model)
            {
                await SetInitialDataAsync();
                if (ModelState.IsValid)
                {
                    UserDTO userDto = new UserDTO
                    {
                        Email = model.Email,
                        Password = model.Password,
                        //Address = model.Address,
                        //Name = model.Name,
                        Role = "user"
                    };
                    OperationDetails operationDetails = await userService.Create(userDto);
                    if (operationDetails.Succedeed)
                        return View("SuccessRegister");
                    else
                        ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
                }
                return View(model);
            }
            private async Task SetInitialDataAsync()
            {
                await userService.SetInitialData(new UserDTO
                {
                    Email = "admin@mail.ru",
                    UserName = "admin@mail.ru",
                    Password = "admin",
                    Name = "Admin Admin",
                    Address = " ",
                    Role = "admin",
                }, new List<string> { "user", "admin" });
            }
        }
    }
