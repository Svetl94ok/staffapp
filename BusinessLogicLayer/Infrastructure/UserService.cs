﻿using BusinessLogicLayer.DataTransferObjects;
using BusinessLogicLayer.Interfaces;
using DataLayer.Entities;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Linq;
using DataLayer.Repositories;

namespace BusinessLogicLayer.Infrastructure
{
    public class UserService : IUserService
    {
        IUnitOfWork Database { get; set; }


        public UserService(IUnitOfWork uow)
        {
            this.Database = uow;
        }


        public async Task<OperationDetails> Create(UserDTO userDto)
        {
            User user = await Database.UserManager.FindByEmailAsync(userDto.Email);
            if (user == null)
            {
                user = new User { Email = userDto.Email, UserName = userDto.Email };
                var result = await Database.UserManager.CreateAsync(user, userDto.Password);
                if (result.Errors.Count() > 0)
                    return new OperationDetails(false, result.Errors.First().ToString(), "");
                // добавляем роль
                await Database.UserManager.AddToRoleAsync(user, userDto.Role);
                // создаем профиль клиента
                UserProfile clientProfile = new UserProfile { Id = user.Id, Address = userDto.Address, Name = userDto.Name };
                Database.ClientManager.Create(clientProfile);
                await Database.SaveAsync();
                return new OperationDetails(true, "User is successfully registrated", "");
            }
            else
            {
                return new OperationDetails(false, "User with the same login already exist", "Email");
            }
        }

        //public async Task<ClaimsIdentity> Authenticate(UserDTO userDto)
        //{
        //    ClaimsIdentity claim = null;
        //    // search user
        //    User user = await Database.UserManager.FindByLoginAsync(userDto.Email, userDto.Password);
        //    // авторизуем его и возвращаем объект ClaimsIdentity
        //    if (user != null)
        //        claim = await Database.UserManager.ConfirmEmailAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
        //    return claim;
        //}

        // начальная инициализация бд
        public async Task SetInitialData(UserDTO adminDto, List<string> roles)
        {
            foreach (string roleName in roles)
            {
                var role = await Database.RoleManager.FindByNameAsync(roleName);
                if (role == null)
                {
                    role = new Role { Name = roleName };
                    await Database.RoleManager.CreateAsync(role);
                }
            }
            await Create(adminDto);
        }


        public async Task<OperationDetails> Read(UserDTO userDto)
        {
            User user = await Database.UserManager.FindByEmailAsync(userDto.Email);
            if (user == null)
            {
                
                Database.ClientManager.Read(user.UserProfile);
                return new OperationDetails(true, "User is successfully read", "");
            }
            else
            {
                return new OperationDetails(false, "User with such email does not exist", "Email");
            }
        }

        public async Task<OperationDetails> Update(UserDTO userDto)
        {
            User user = await Database.UserManager.FindByEmailAsync(userDto.Email);
            if (user == null)
            {

                Database.ClientManager.Update(user.UserProfile);
                return new OperationDetails(true, "User is successfully updated", "");
            }
            else
            {
                return new OperationDetails(false, "User with such email does not exist", "Email");
            }
        }

        public async Task<OperationDetails> Delete(UserDTO userDto)
        {
            User user = await Database.UserManager.FindByEmailAsync(userDto.Email);
            if (user == null)
            {

                Database.ClientManager.Delete(user.Email);
                return new OperationDetails(true, "User is successfully updated", "");
            }
            else
            {
                return new OperationDetails(false, "User with such email does not exist", "Email");
            }
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}