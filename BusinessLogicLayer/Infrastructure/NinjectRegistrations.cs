﻿using BusinessLogicLayer.Interfaces;
using Ninject.Modules;

namespace BusinessLogicLayer.Infrastructure
{
    public class NinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserService>().To<UserService>();
        }
    }
}
