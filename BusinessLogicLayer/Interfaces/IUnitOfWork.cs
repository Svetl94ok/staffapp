﻿
using DataLayer.Identity;
using DataLayer.Interfaces;
using System;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IClientManager ClientManager { get; }
        Task SaveAsync();
        AppUserManager UserManager { get; }
        AppRoleManager RoleManager { get; }
    }
}
