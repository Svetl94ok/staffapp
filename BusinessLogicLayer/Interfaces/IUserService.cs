﻿using BusinessLogicLayer.DataTransferObjects;
using BusinessLogicLayer.Infrastructure;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Interfaces
{
    interface IUserService
    {
        Task<OperationDetails> Create(UserDTO userDto);
        Task<OperationDetails> Read(UserDTO userDto);
        Task<OperationDetails> Update(UserDTO userDto);
        Task<OperationDetails> Delete(UserDTO userDto);
        //Task<ClaimsIdentity> Authenticate(UserDTO userDto);
        Task SetInitialData(UserDTO adminDto, List<string> roles);
    }
}
