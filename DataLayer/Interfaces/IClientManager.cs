﻿using DataLayer.Entities;
using System;

namespace DataLayer.Interfaces
{
    public interface IClientManager : IDisposable
    {
        void Create(UserProfile item);
        void Delete(string email);
        void Update (UserProfile item);
        UserProfile Read(UserProfile item);
        User GetUserByID(int id);

    }
}
