﻿using DataLayer.Entities;
using DataLayer.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;


namespace DataLayer.Repositories
{
    public class ClientManager : IClientManager
    {
        public ApplicationContext Database { get; set; }
        public ClientManager(ApplicationContext db)
        {
            Database = db;
        }

        public void Create(UserProfile item)
        {
            Database.UserProfile.Add(item);
            Database.SaveChanges();
        }

        public User GetUserByID(int id)
        {
            return Database.Users.Find(id);
        }

        void IClientManager.Delete(string email)
        {
            User user = Database.Users.Find(email);
            Database.Update(user.IsActive = false);
            Database.SaveChanges();
        }

        void IClientManager.Update(UserProfile item)
        {

            Database.Entry(item).State = EntityState.Modified;
            Database.SaveChanges();

        }

        UserProfile IClientManager.Read(UserProfile item)
        {
            return item;
        }
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Database.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}  

