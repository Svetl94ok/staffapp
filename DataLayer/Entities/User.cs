﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DataLayer.Entities
{
    public class User : IdentityUser 
    {
        public virtual UserProfile UserProfile { get; set; }
        public bool IsActive { get; set; }
        public Company Company { get; set; }
    }
}
