﻿using DataLayer.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataLayer.Identity
{

    public class AppRoleManager : RoleManager<Role>
    {
        //public ApplicationRoleManager(RoleStore<ApplicationRole> store)
        //            : base(store)
        //{ }
        public AppRoleManager(IRoleStore<Role> store, IEnumerable<IRoleValidator<Role>> roleValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, Microsoft.Extensions.Logging.ILogger<RoleManager<Role>> logger) : base(store, roleValidators, keyNormalizer, errors, logger)
        {
        }
    }
}

